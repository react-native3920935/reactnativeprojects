import React, { useState } from 'react';
import { StyleSheet, Text, TextInput, View} from 'react-native';

export default function App(){
  const[name, setName]= useState('Devika S Das');
  const[age,   setAge]= useState('22');

  return(
    <View style={styles.container}>
      <Text> Enter the name: </Text>
      <TextInput  
      multiline
      style={styles.input}
      placeholder='eg . Anjana'
      onChangeText={(val)=> setName(val)}/>

      <Text> Enter the age: </Text>
      <TextInput 
      keyboardType='numeric'
      style={styles.input}
      placeholder='eg . 24'
      onChangeText={(val)=> setAge(val)}/>
      <Text>name :{name} , age: {age}   </Text>
    </View>
  );
}
 const styles =StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'green',
    alignItems:'center',
    justifyContent:'center',
 },
 
 input: {
  borderWidth:1,
  borderColor:'blue',
  padding:10,
  margin:20,
  width:250,

 },
});