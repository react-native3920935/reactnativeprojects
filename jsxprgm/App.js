import React from "react";
import { Text, View, Button } from "react-native";


const App =()=> {
  let name ='DEVIKA'; 
  let lastName = 'DAS'
  const age =22;
  return (
    <View>
      <Text style={{fontSize:20}}> JSX In React Native</Text>
      <Text style={{fontSize:10}}> {name}</Text>
      <Text style={{fontSize:10}}> {age}</Text>
      <Text style={{fontSize:10}}> {10/10}</Text>
      <Text style={{fontSize:10}}> {name} {lastName}</Text>
      <Text style={{fontSize:10}}> {name}  {lastName} </Text>
      <Text style={{fontSize:10}}> {age<20?"you are underage":"age is not matching"}</Text>

    </View>
  )
}
export default App;