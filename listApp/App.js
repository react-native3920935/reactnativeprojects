

import React, { useState } from 'react';
import { StyleSheet,Text, View, FlatList,TouchableOpacity } from 'react-native';
 export default function App (){
  const [people,setPeople] =useState([
    { name: 'Devika', key:'1'},
    { name: 'Krithika', key:'2'},
    { name: 'Anjana', key:'3'},
    { name: 'Keerthna', key:'4'},
    { name: 'Rahul', key:'5'},
    { name: 'Aparna', key:'6'},
    { name: 'Riya', key:'7'},
    { name: 'Diya', key:'98'},
  ]);

  const pressHandler=(key)=>{
    console.log(key);
  }
 
 return (
  <View style={styles.container}>

    <FlatList 
    numColumns={2}
    keyExtractor={(item)=> item.key }
    data={people}
    renderItem = { ({item})=>(
      <TouchableOpacity  onPress={()=> pressHandler(item.key) }>
        <Text style= {styles.item}>{ item.name}</Text>
        </TouchableOpacity>
      )}
      />

  {/*<ScrollView>
  { people.map((item) => {
      return(
        <View key={item.key}>
          <Text style={styles.item} > {item.name}   </Text>
        </View>
      )
  })}
</ScrollView>*/}
  </View>
 );
}
 
const styles= StyleSheet.create({
  container: {
    flex:1 ,
  backgroundColor:'white',
  padding:30,
  paddingHorizontal:20,
  alignItems:'center',
  justifyContent:'center',
  },

  item:{
    marginTop:28,
    padding:10,
    backgroundColor:'green',
    fontSize:20,
  }
});
