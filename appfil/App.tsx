import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const App = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.Text}>my pet jaku</Text>
    </View>
  );
};
export default App;

const styles =StyleSheet.create({
  Text: {
    fontSize:34,
    color:'blue',
  },
  container:  {
    flex:1,
    justifyContent:'center',
    alignItems:'center',
    color:'white',
  },
});