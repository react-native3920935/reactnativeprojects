import React, {useState} from 'react';
import {Text, TextInput, View, StyleSheet, Button} from 'react-native';
const App = () => {
  const [name, setName] = useState('');
  const [email, setemail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <View>
      <Text style={{fontSize: 30}}> Simple Form in React Native</Text>
      <TextInput
        placeholder="Enter User Name"
        onChangeText={text => {
          setName(text);
        }}
        value={name}
      />

      <TextInput
        placeholder="Enter Password"
        secureTextEntry={true}
        onChangeText={text => {
          setPassword(text);
        }}
        value={password}
      />

      <TextInput
        placeholder="Enter email"
        onChangeText={text => {
          setemail(text);
        }}
        value={email}
      />

      <View style={{marginBottom: 10}}>
        <Button title="Get Details" />
      </View>
      <Button title="Clear Details" />
    </View>
  );
};

const styles = StyleSheet.create({
  textInput: {
    fontSize: 18,
    color: 'blue',
  },
});

export default App;
