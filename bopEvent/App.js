import React from "react";
import { Button,Text,View } from "react-native"
const App =() => {
  const fruit = (val)=> {
  console.warn (val);
}
  return  (
    <View>
      <Text style ={{fontSize:20}}>Button with onPress Event</Text>
      <Button title ="On press" onPress={fruit} color={'green'} />
      <Button title ="On press" onPress={()=>fruit('HAVE A GOOD DAY')} color={'red'} />
      </View>
  );
};
export default App;
