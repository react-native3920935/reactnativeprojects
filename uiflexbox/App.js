import React, { useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Button,
  ScrollView,
  RefreshControl,
} from 'react-native';

const App = () => {
  const [Items, setItems] = useState([
    { key: 1, item: 'Item 0' },
    { key: 2, item: 'Item 2' },
    { key: 3, item: 'Item 6' },
    { key: 4, item: 'Item 8' },
    { key: 5, item: 'Item 10' },
    { key: 6, item: 'Item 12' },
    { key: 7, item: 'Item 14 '},
    { key: 8, item: 'Item 16' },
    { key: 9, item: 'Item 18' },
    { key: 10, item: 'Item 20' },
    { key: 11, item: 'Item 22' },
    { key: 12, item: 'Item 24' },
        // ... other items ...
  ]);
  const [Refreshing, setRefreshing] = useState(false);

  const onRefresh = () => {
    setRefreshing(true);
    setItems([...Items, { key: 69, item: 'Item 69' }]);
    setRefreshing(false);
  };

  return (
    <ScrollView
      style={styles.body}
      refreshControl={
        <RefreshControl refreshing={Refreshing} onRefresh={onRefresh} colors={['green']} />
      }>
      {Items.map((item) => (
        <View style={styles.item} key={object.key}>
          <Text style={styles.text}>{object.item}</Text>
        </View>
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  body: {
    flex: 1,
  },
  item: {
    padding: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  text: {
    fontSize: 16,
  },
});

export default App;