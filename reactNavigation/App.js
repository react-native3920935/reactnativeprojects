import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Stack from './Navigation/Stack';
import Navi from './Navigation/Navi';
const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Stack"
          component={Stack}
          options={{title: 'Welcome'}}
        />
        <Stack.Screen name="Navi" component={Navi} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;