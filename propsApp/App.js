import React, { useState } from 'react';
import { Button, Text, View } from 'react-native'
const App=()=>{
const [name,setName]=useState("Evaans")
  return(
    <View>
      <Text style={{fontSize:30 }}>Props in React Native</Text>
      <Button title ='UpdateName' onPress={() =>setName("Chris Hermsworth")}/> 
      <User name={name}age={29}/>
    </View>
  );
};

const User=(props)=>{
  console.warn(props.name)
  return(
    <View style ={{backgroundColor:'blue'}}>
      <Text style={{fontSize:30 }}>{props.name}</Text>
      <Text style={{fontSize:30 }}>{props.age}</Text>
    </View>
  )
}




export default App;
