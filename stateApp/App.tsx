import React, { useState } from "react";
import { Button, Text, View } from 'react-native'
const App = () => {
  const [name,setName] =useState("Dev");
  //let data ='DEVZ';

  const testUpdate=() =>{
    setName("Devzzz")
  }
  return (
    <View>
      <Text style ={{fontSize:30}}> {name}</Text>
      <Button title="Update state" onPress={testUpdate} />
      {/*<Text style ={{fontSize:30}} > {data}</Text> */}
      
    </View> 
  );
};


export default App;