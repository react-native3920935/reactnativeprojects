import React ,{useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Button,
  Linking,
} from 'react-native';



const App = ()=>{
const [name,setName]= useState('CODESAP')
const[session,setSession]= useState({number:6,title:'state'})
const [current,setCurrent]=useState(true)

const onClickHandler =()=>{
  setName('Codesap')
  setSession({number:7,title:'Style'})

}
  return (
    <View style ={styles.body}>
      <Text style ={styles.text}>Codesap</Text>
      <Text style ={styles.text}>This is session number{session.number} and about {session.number}</Text>
      <Text style ={styles.text}>{current ?'current session': 'next session'}</Text>
      <Button title='Update State' onPress={onClickHandler}></Button>
      </View>
  );
};
  

  const styles= StyleSheet.create({
    body:{
      flex:1,
      backgroundColor:'white',
      alignItems:'center',
      justifyContent:'center'
    },
    text: {
      color:'green',
      fontSize:20,
      fontStyle:'italic',
      margin: 10,
    },
  });

export default App; 